<?php


namespace App\Service\Event;


use App\Entity\CartItem;
use Symfony\Contracts\EventDispatcher\Event;

class AddToCart extends Event
{
    public const NAME = 'cart.add';

    protected $cartItem;

    public function __construct(CartItem $cartItem)
    {
        $this->cartItem = $cartItem;
    }

    /**
     * @return CartItem
     */
    public function getCartItem(): CartItem
    {
        return $this->cartItem;
    }


}