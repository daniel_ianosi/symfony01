<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam;

/**
 * @Route("/api/category")
 */
class CategoryApiController extends AbstractFOSRestController
{
    /**
     *
     * @Route("/{id}", name="category_api_get", methods={"GET"})
     */
    public function index(Request $request, CategoryRepository $categoryRepository, $id=0): Response
    {
        try {
            if ($id == 0) {
                $data = $categoryRepository->findBy($request->query->get('filter', []), $request->query->get('orderBy', null), $request->query->get('limit', 10), $request->query->get('offset', null));
            } else {
                $data = $categoryRepository->find($id);
            }
            $view = $this->view($data, 200);
        } catch (\Exception $ex){
            $view = $this->view(['message'=>$ex->getMessage()], 404);
        }

        return $this->handleView($view);
    }

    /**
     * @Route("/", name="category_api_post", methods={"POST"})
     */
    public function post(CategoryRepository $categoryRepository, $id=0): Response
    {
        $category = new Category();
        $data = $this->createForm(CategoryType::class, $category);
        $view = $this->view($data, 200);

        //dd($data->getData());

        return $this->handleView($view);
    }
}
