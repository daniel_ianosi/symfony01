<?php


namespace App\Service;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class CartService
{
    /** @var Cart */
    private $cart;

    /** @var Security */
    private $security;

    /** @var SessionInterface */
    private $session;

    /** @var EntityManagerInterface */
    private $em;


    public function __construct(Security $security, SessionInterface $session, EntityManagerInterface $em)
    {
        $this->security = $security;
        $this->session = $session;
        $this->em = $em;
    }


    public function getCart():Cart
    {
        if ($this->getSession()->has('cart_id')) {
            $this->cart = $this->em->getRepository(Cart::class)->find($this->getSession()->get('cart_id'));
        } else {
            $this->cart = new Cart();
            $this->cart->setUser($this->getSecurity()->getUser());
            $this->cart->setStatus(Cart::NEW_STATUS);
            $this->em->persist($this->cart);
            $this->em->flush();
            $this->getSession()->set('cart_id',  $this->cart->getId());
        }

        return $this->cart;
    }

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function add(Product $product, $quantity=1)
    {
        $cart = $this->getCart();

        $cartItem = $this->em->getRepository(CartItem::class)->findOneBy(['cart'=>$cart, 'product'=>$product]);
        if ($cartItem) {
            $cartItem->setQuantity($cartItem->getQuantity()+$quantity);
        } else {
            $cartItem = new CartItem();
            $cartItem->setProduct($product)
                ->setQuantity($quantity)
                ->setCart($cart);
        }

        $this->em->persist($cartItem);
        $this->em->flush();

        return $cartItem;
    }

    public function update(CartItem $cartItem, $quantity)
    {
        $cartItem->setQuantity($quantity);
        $this->em->persist($cartItem);
        $this->em->flush();
    }

    public function remove(CartItem $cartItem)
    {
        $this->em->remove($cartItem);
        $this->em->flush();
    }

    public function clear()
    {
        $cart = $this->getCart();
        $this->em->remove($cart);
        $this->em->flush();
    }

    /**
     * @return Security
     */
    public function getSecurity(): Security
    {
        return $this->security;
    }

    /**
     * @param Security $security
     * @return CartService
     */
    public function setSecurity(Security $security): CartService
    {
        $this->security = $security;
        return $this;
    }

    /**
     * @return SessionInterface
     */
    public function getSession(): SessionInterface
    {
        return $this->session;
    }

    /**
     * @param SessionInterface $session
     * @return CartService
     */
    public function setSession(SessionInterface $session): CartService
    {
        $this->session = $session;
        return $this;
    }


}