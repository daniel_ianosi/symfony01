<?php

namespace App\Command;

use App\Entity\Cart;
use App\Service\InvoiceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateInvoicesCommand extends Command
{
    protected static $defaultName = 'app:generate-invoices';

    /** @var EntityManagerInterface */
    private $em;

    /** @var InvoiceService */
    private $invoiceService;

    /**
     * GenerateInvoicesCommand constructor.
     * @param EntityManagerInterface $em
     * @param InvoiceService $invoiceService
     */
    public function __construct(string $name = null, EntityManagerInterface $em, InvoiceService $invoiceService)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->invoiceService = $invoiceService;
    }


    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        /** @var Cart[] $carts */
        $carts = $this->em->getRepository(Cart::class)->findBy(['status'=>Cart::FINALISED_STATUS], null, 100);

        foreach ($carts as $cart){
            $this->invoiceService->generateInvoice($cart);
            $cart->setStatus(Cart::INVOICED_STATUS);
            $this->em->persist($cart);
            $io->success('Invoice created for cart:'.$cart->getId());
        }
        $this->em->flush();

        return 0;
    }
}
