<?php


namespace App\Validator\Constraints;


use Doctrine\Common\Annotations\Annotation\Target;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MyConstraint extends Constraint
{
    public $message = 'This is my error message';

    public function validatedBy()
    {
        return MyValidator::class;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}