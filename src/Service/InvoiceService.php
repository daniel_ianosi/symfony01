<?php


namespace App\Service;


use App\Entity\Cart;
use Symfony\Bridge\Twig\TwigEngine;
use Twig\Environment;
use WhiteOctober\TCPDFBundle\Controller\TCPDFController;

class InvoiceService
{
    /** @var TCPDFController */
    private $tcpdf;

    /** @var Environment */
    private $twig;

    /**
     * InvoiceService constructor.
     * @param TCPDFController $tcpdf
     * @param Environment $twig
     */
    public function __construct(TCPDFController $tcpdf, Environment $twig)
    {
        $this->tcpdf = $tcpdf;
        $this->twig = $twig;
    }


    public function generateInvoice(Cart $cart, $format='PDF')
    {
        if ($format=='HTML'){
           $a=1;
        }


        if (count($cart->getCartItems())==0){
            throw new \Exception("Invoice can't be empty");
        }
        // create new PDF document
        $pdf = $this->tcpdf->create();


// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set default font subsetting mode
        $pdf->setFontSubsetting(true);



// set font
        $pdf->SetFont('dejavusans', '', 10);

// add a page
        $pdf->AddPage();

// create some HTML content
        $html = $this->twig->render('cart/invoice.html.twig', [
            'cart' => $cart
        ]);

// output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        //dd(__DIR__);
        $pdf->Output(__DIR__.'/../../public/invoices/invoice'.$cart->getId().'.pdf', 'F');
    }
}