<?php


namespace App\Service\Deliverer;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class DelivererCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $serviceIds = $container->findTaggedServiceIds('deliverer');

        $orderServiceDefinition = $container->getDefinition('order_service');
        //$orderService = $container->get('order_service');


        foreach ($serviceIds as $serviceId => $data){
            $orderServiceDefinition->addMethodCall('addDeliverer',[$container->getDefinition($serviceId)]);
        }
    }
}