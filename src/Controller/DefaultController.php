<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class DefaultController
 * @Route ("/emag")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/home/{name}", name="homepage")
     */
    public function index(Request $request): Response
    {
        return $this->render('default/index.html.twig', [
            'name' => 'assa',
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/lang/{lang}", name="lang")
     */
    public function lang(Request $request, $lang, Session $session):Response
    {
        $session->set('_locale', $lang);

        return $this->redirectToRoute('product_index');
    }

    /**
     * @Route("/category/{id}", name="category")
     */
    public function category(Category $category, Request $request): Response
    {
        $category = new Category();
        $form = $this->createFormBuilder($category)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
        }


        return $this->render('default/index.html.twig', [
            'name' => $category->getName(),
            'myName' => $form->createView(),
        ]);
    }

    /**
     * @Route("/product/new", name="new_product")
     */
    public function product(Request $request, ValidatorInterface $validator): Response
    {
        $em = $this->getDoctrine()->getManager();
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){

            $em->persist($product);
            $em->flush();
        }

        return $this->render('default/index.html.twig', [
            'name' => $product->getName(),
            'myName' => $form->createView(),
        ]);
    }
}
