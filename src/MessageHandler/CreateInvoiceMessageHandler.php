<?php


namespace App\MessageHandler;

use App\Entity\Cart;
use App\Message\CreateInvoice;
use App\Service\InvoiceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateInvoiceMessageHandler implements MessageHandlerInterface
{
    /** @var InvoiceService */
    private $invoiceService;

    /** @var EntityManagerInterface */
    private $em;

    /**
     * CreateInvoiceMessageHandler constructor.
     * @param InvoiceService $invoiceService
     * @param EntityManagerInterface $em
     */
    public function __construct(InvoiceService $invoiceService, EntityManagerInterface $em)
    {
        $this->invoiceService = $invoiceService;
        $this->em = $em;
    }


    public function __invoke(CreateInvoice $message)
    {
        $cart = $this->em->getRepository(Cart::class)->find($message->getCartId());
        $this->invoiceService->generateInvoice($cart);
    }
}