<?php


namespace App\Service\Deliverer;


use App\Repository\CartRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BaseDeliverer
{
    /** @var SessionInterface */
    private $session;

    /** @var CartRepository */
    private $cartRepository;

    /**
     * BaseDeliverer constructor.
     * @param SessionInterface $session
     * @param CartRepository $cartRepository
     */
    public function __construct($session, $cartRepository)
    {
        $this->session = $session;
        $this->cartRepository = $cartRepository;
    }


}