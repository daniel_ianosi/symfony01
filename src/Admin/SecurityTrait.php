<?php


namespace App\Admin;
use App\Entity\BaseEntity;
use Symfony\Component\Security\Core\Security;

trait SecurityTrait
{
    /** @var Security */
    private $security;

    /**
     * @param BaseEntity $object
     */
    public function prePersist($object)
    {
        $object->setOwner($this->getSecurity()->getUser());
        parent::prePersist($object); // TODO: Change the autogenerated stub
    }

    /**
     * @return Security
     */
    public function getSecurity(): Security
    {
        return $this->security;
    }

    /**
     * @param Security $security
     * @return ProductAdmin
     */
    public function setSecurity(Security $security): ProductAdmin
    {
        $this->security = $security;
        return $this;
    }

}