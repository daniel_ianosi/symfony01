<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/product")
 */
class ProductApiController extends AbstractController
{
    /**
     * @Route("/{id}", name="product_api_get", methods={"GET"})
     */
    public function getProduct(ProductRepository $productRepository, SerializerInterface $serializer, $id=0): Response
    {
        if ($id==0){
            $data = $productRepository->findAll();
        } else {
            $data = $productRepository->find($id);
        }

        $jsonString = $serializer->serialize($data, 'json');
        return new Response($jsonString);
    }

    /**
     * @Route("/", name="product_api_post", methods={"POST"})
     */
    public function post(SerializerInterface $serializer, EntityManagerInterface $entityManager, Request $request)
    {
        $product = $serializer->deserialize($request->getContent(), Product::class, 'json');
        $entityManager->persist($product);
        $entityManager->flush();

        $jsonString = $serializer->serialize($product, 'json');
        return new Response($jsonString);
    }

    /**
     * @Route("/", name="product_api_put", methods={"PUT"})
     */
    public function put(SerializerInterface $serializer, EntityManagerInterface $entityManager, Request $request)
    {
        $product = $serializer->deserialize($request->getContent(), Product::class, 'json');
        $entityManager->merge($product);
        $entityManager->flush();

        $jsonString = $serializer->serialize($product, 'json');
        return new Response($jsonString);
    }

    /**
     * @Route("/{id}", name="product_api_patch", methods={"PATCH"})
     */
    public function patch(Product $product, SerializerInterface $serializer, EntityManagerInterface $entityManager, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        foreach ($data as $key => $value){
            $setter = 'set'.ucfirst($key);
            $product->$setter($value);
        }

        $entityManager->persist($product);
        $entityManager->flush();

        $jsonString = $serializer->serialize($product, 'json');
        return new Response($jsonString);
    }

    /**
     * @Route("/{id}", name="product_api_delete", methods={"DELETE"})
     */
    public function delete(Product $product, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($product);
        $entityManager->flush();

        $jsonString = '[]';
        return new Response($jsonString);
    }
}
