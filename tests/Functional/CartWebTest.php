<?php


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CartWebTest extends WebTestCase
{
    public function testShowCart()
    {
        $client = static::createClient();

        $client->request('GET', '/cart');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }


    public function testAddToCart()
    {
        $client = static::createClient();

        $request = $client->request('GET', '/cart/6/add/6');


        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->request('GET', '/cart');

        $this->assertSelectorTextContains('#itemQuantity', '6');
    }

    public function testAddToCartInvalidProduct()
    {
        $client = static::createClient();

        $client->request('GET', '/cart/50/add/4');

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}