<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MyValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        if ($value->getName() == 'Daniel' && $value->getPrice()==-45){
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}