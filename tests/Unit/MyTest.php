<?php

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;
use App\Service\InvoiceService;
use Twig\Environment;
use WhiteOctober\TCPDFBundle\Controller\TCPDFController;

class MyTest extends TestCase
{
    public function testInvoiceService()
    {
        $invoiceService = $this->createInvoiceService();
        $this->assertInstanceOf(InvoiceService::class, $invoiceService);
    }

    public function testCreatePDFInvoice()
    {
        $invoicePath = __DIR__ . '/../../public/invoices/invoice.pdf';
        if (file_exists($invoicePath)){
            unlink($invoicePath);
        }

        $invoiceService = $this->createInvoiceService();

        $cart = new Cart();
        $cartItem = new CartItem();
        $product = new Product();
        $product->setName('Produs de test');
        $product->setPrice(29.99);
        $cartItem->setProduct($product);
        $cartItem->setQuantity(5);
        $cart->addCartItem($cartItem);

        $invoiceService->generateInvoice($cart);
        $this->assertFileExists($invoicePath.'12344');
    }

    public function testCreateEmptyInvoice()
    {
        $invoicePath = __DIR__ . '/../../public/invoices/invoice.pdf';
        if (file_exists($invoicePath)){
            unlink($invoicePath);
        }

        $invoiceService = $this->createInvoiceService();

        $cart = new Cart();

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Invoice can't be empty");
        $invoiceService->generateInvoice($cart);
    }

    public function testCreatePDFInvoiceHTMLFormat()
    {
        $invoicePath = __DIR__ . '/../../public/invoices/invoice.pdf';
        if (file_exists($invoicePath)){
            unlink($invoicePath);
        }

        $invoiceService = $this->createInvoiceService();

        $cart = new Cart();
        $cartItem = new CartItem();
        $product = new Product();
        $product->setName('Produs de test');
        $product->setPrice(29.99);
        $cartItem->setProduct($product);
        $cartItem->setQuantity(5);
        $cart->addCartItem($cartItem);

        $invoiceService->generateInvoice($cart,'HTML');
        $this->assertFileExists($invoicePath);
    }



    /**
     * @return InvoiceService
     */
    public function createInvoiceService(): InvoiceService
    {
        $tcpdf = new TCPDFController('TCPDF');
        $loader = new Twig\Loader\FilesystemLoader(__DIR__ . '/../../templates/');
        $twig = new Environment($loader, array(
            'cache' => '/var/tmp/journeymonitor-twig-tests-cache',
        ));

        $invoiceService = new InvoiceService($tcpdf, $twig);

        return $invoiceService;
    }
}