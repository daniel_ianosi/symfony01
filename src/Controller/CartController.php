<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Product;
use App\Message\CreateInvoice;
use App\Service\CartService;
use App\Service\Event\AddToCart;
use App\Service\InvoiceService;
use App\Service\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use WhiteOctober\TCPDFBundle\Controller\TCPDFController;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="cart_index")
     */
    public function index(CartService $cartService): Response
    {
        return $this->render('cart/index.html.twig', [
            'cart' => $cartService->getCart()
        ]);
    }

    /**
     * @Route("/cart/{id}/add/{quantity}", name="cart_add")
     */
    public function add(Request $request, CartService $cartService, EventDispatcherInterface $dispatcher,  Product $product, $quantity=1)
    {
        $cartItem = $cartService->add($product, $quantity);

        $event = new AddToCart($cartItem);

        $dispatcher->dispatch($event, AddToCart::NAME);

        return $this->redirect($this->generateUrl('cart_index'));
    }

    /**
     * @Route("/cart/{id}/finalize", name="cart_finalize")
     */
    public function finalize(EntityManagerInterface $em, Cart $cart, SessionInterface $session)
    {
        $cart->setStatus(Cart::FINALISED_STATUS);
        $em->persist($cart);
        $em->flush();

        $session->remove('cart_id');

        $this->dispatchMessage(new CreateInvoice($cart->getId()));

        return $this->redirectToRoute('cart_index');
    }

    /**
     * @Route("/cart/{id}/invoice", name="cart_invoice")
     */
    public function invoice(Cart $cart, InvoiceService $invoiceService)
    {
        $invoiceService->generateInvoice($cart);
        return $this->redirectToRoute('cart_index');
    }
}
