<?php


namespace App\Service\Listener;

use App\Entity\BaseEntity;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PersistListener
{
    /** @var ValidatorInterface */
    private $validator;

    /** @var Security */
    private $security;

    /**
     * PersistListener constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator, Security $security)
    {
        $this->validator = $validator;
        $this->security = $security;
    }


    public function prePersist(LifecycleEventArgs $eventPayload){
        $violations = $this->validator->validate($eventPayload->getObject());
        foreach ($violations as $violation){
            throw new \Exception($violation->getMessage());
        }

        if ($eventPayload->getObject() instanceof BaseEntity){
            $eventPayload->getObject()->setOwner($this->security->getUser());
        }
    }

    public function preUpdate(LifecycleEventArgs $eventPayload){
        $this->prePersist($eventPayload);
    }
}