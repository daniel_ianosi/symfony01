<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCreateCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:user:create';

    //private $passwordEncoder;

    //private $em;

    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }


    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('email', InputArgument::OPTIONAL, 'Email description')
            ->addArgument('password', InputArgument::OPTIONAL, 'Password description')
            ->addArgument('roles', InputArgument::IS_ARRAY, 'Roles description')
            ->addOption('all-rolls', 'a', InputOption::VALUE_NONE, 'Option all rolls')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        if ($input->getArgument('email')){
            $email = $input->getArgument('email');
        } else {
            $email = $io->ask('Email:');
        }

        if ($input->getArgument('password')){
            $password = $input->getArgument('password');
        } else {
            $helper = $this->getHelper('question');
            $question = new Question('Password:');
            $question->setHidden(true);
            $question->setHiddenFallback(false);

            $password = $helper->ask($input, $output, $question);
        }

        if ($input->getArgument('roles')){
            $roles = $input->getArgument('roles');
        } else {
            $roles =[];
            $role=' ';
            while($role != ''){
                $role = $io->ask('Add role:');
                if ($role){
                    $roles[]=$role;
                }
            }
        }


        if ($input->getOption('all-rolls')){
            $roles = ['user','admin','superadmin'];
        }

        $em=$this->getContainer()->get('doctrine.orm.default_entity_manager');
        $passwordEncoder=$this->getContainer()->get('security.password_encoder');

        $user = new User();
        $user->setEmail($email);
        $user->setRoles($roles);
        $user->setPassword($passwordEncoder->encodePassword($user, $password));

        try {
            $em->persist($user);
            $em->flush();
            $io->success('User created successfully.');
        } catch (\Exception $ex){
            if ($ex instanceof UniqueConstraintViolationException){
                $io->error('User allready exists!');
            } else {
                $io->error($ex->getMessage());
            }
        }

        return 0;
    }
}
