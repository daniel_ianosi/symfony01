<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Repository\CartRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use OpenApi\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;


class ApiController extends AbstractController
{
    /**
     * * List the rewards of the specified user.
     *
     * This call takes into account all confirmed awards, but not pending or refused awards.
     *  @SWG\Response(
     *     description="",
     *     response=200,
     *     @Model(type=Cart::class, groups={"non_sensitive_data"})
     * )
     * @Route("/api/getCarts/{page}", name="api_get_carts", methods={"GET"})
     */
    public function index(CartRepository $cartRepository, SerializerInterface $serializer, $page=1): Response
    {

        $carts = $cartRepository->getPage($page, 1);

        $jsonString = $serializer->serialize($carts, 'json');

        return new Response($jsonString);
    }

    /**
     * @Route("/api/getCourencies", name="api_get_courencies")
     */
    public function getCourencies(HttpClientInterface $client)
    {
        $response = $client->request(
            'GET',
            'https://www.smsgateway.ro/nbrfxrates.xml'
        );

        $encoders = [new XmlEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $data = $serializer->decode($response->getContent(),  'xml');

        return new JsonResponse($data);
    }
}
