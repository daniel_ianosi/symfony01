<?php

namespace App\Entity;

use App\Repository\ImageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ImageRepository::class)
 */
class Image  extends BaseEntity
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $file;

    /**
     * @Assert\Image(
     *     minWidth = 200,
     *     maxWidth = 10000,
     *     minHeight = 200,
     *     maxHeight = 10000
     * )
     */
    private $upload;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="imagini")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible;


    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /**
     * @param UploadedFile $upload
     * @return Image
     */
    public function setUpload($upload)
    {
        $this->setFile($upload->getClientOriginalName());
        $this->upload = $upload;
        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

}
