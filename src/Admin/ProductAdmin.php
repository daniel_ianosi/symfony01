<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\BaseEntity;
use App\Repository\CategoryRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class ProductAdmin extends AbstractAdmin
{
    use OwnershipTrait;

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('price')
            ->add('image')
            ->add('images')
            ->add('category')
            ->add('updatedAt')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('category')
            ->add('name', null, ['editable' => true])
            ->add('price', null, ['editable' => true])
            ->add('image')
            ->add('images')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                    'my_action' => ['template' => 'admin\my_action.html.twig']
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $user = $this->getSecurity()->getUser();
        $formMapper
            ->add('name')
            ->add('price')
            ->add('image')
            ->add('category',
            null,
            [
                'class' => 'App\Entity\Category',
                'query_builder' => function (CategoryRepository $er) use ($user) {
                    return $er->createQueryBuilder('o')
                        ->where('o.owner = :owner')
                        ->setParameter('owner', $user)
                        ->orderBy('o.name', 'ASC');
                },
            ]
            )
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('name')
            ->add('price')
            ->add('image')
            ->add('images')
            ;
    }




}
