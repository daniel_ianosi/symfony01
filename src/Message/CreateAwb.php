<?php


namespace App\Message;


class CreateAwb
{
    public $cartId;

    /**
     * CreateInvoice constructor.
     * @param $cartId
     */
    public function __construct($cartId)
    {
        $this->cartId = $cartId;
    }

    /**
     * @return mixed
     */
    public function getCartId()
    {
        return $this->cartId;
    }
}