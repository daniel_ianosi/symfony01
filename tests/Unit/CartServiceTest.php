<?php


use App\Entity\Cart;
use App\Service\CartService;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Security;
use \Symfony\Component\HttpFoundation\Session\Session;
use \Doctrine\ORM\EntityManager;

class CartServiceTest extends TestCase
{
    public function testConstructor()
    {
        $cartService = $this->getCartService(false);

        $this->assertInstanceOf(CartService::class, $cartService);
        $this->assertEquals(CartService::class, get_class($cartService));
        $this->assertTrue($cartService instanceof CartService);
    }

    public function testGetCart()
    {
        $cartService = $this->getCartService(true);

        $cart = $cartService->getCart();
        $this->assertInstanceOf(Cart::class, $cart);
    }

    /**
     * @return CartService
     */
    public function getCartService($sessionHasReturn): CartService
    {
        $securityMock = $this->getMockBuilder(Security::class)->disableOriginalConstructor()
            ->getMock();
        $security1Mock = $this->createMock(Security::class);

        $sessionMock = $this->createMock(Session::class);

        $sessionMock->expects($this->any())
            ->method('has')
            ->willReturn($sessionHasReturn);

        $cartRepository = $this->createMock(ObjectRepository::class);

        $cart = new Cart();

        $cartRepository->expects($this->any())
            ->method('find')
            ->willReturn($cart);

        $emMock = $this->createMock(EntityManager::class);
        $emMock->method('getRepository')
            ->willReturn($cartRepository);


        $cartService = new CartService($securityMock, $sessionMock, $emMock);
        return $cartService;
    }
}