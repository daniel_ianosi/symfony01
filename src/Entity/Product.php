<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\MyConstraint;

/**
 * @MyConstraint
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product extends BaseEntity
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="products")
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Assert\All({
     * @Assert\Image(
     *     minWidth = 200,
     *     maxWidth = 10000,
     *     minHeight = 200,
     *     maxHeight = 10000
     * )
     * })
     */
    private $file;

    /**
     * @ORM\Column(type="array")
     */
    private $images = [];

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="product", orphanRemoval=true, cascade={"persist"})
     */
    private $imagini;

    /**
     * @ORM\Column(type="float")
     */
    private $discount;

    public function __construct()
    {
        $this->imagini = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name.'';
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     * @return Product
     */
    public function setFile($file)
    {
        $this->file = $file;

        $fileImages = [];
        foreach ($file as $fileItem){
            $image = new Image();
            $image->setFile($fileItem->getClientOriginalName());
            $this->addImagini($image);
        }
        $this->setImages($fileImages);
        return $this;
    }

    public function getImages(): ?array
    {
        return $this->images;
    }

    public function setImages(array $images): self
    {
        $this->images = $images;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImagini(): Collection
    {
        return $this->imagini;
    }

    public function addImaginus(Image $imagini): self
    {
        if (!$this->imagini->contains($imagini)) {
            $this->imagini[] = $imagini;
            $imagini->setProduct($this);
        }

        return $this;
    }

    public function removeImaginus(Image $imagini): self
    {
        if ($this->imagini->removeElement($imagini)) {
            // set the owning side to null (unless already changed)
            if ($imagini->getProduct() === $this) {
                $imagini->setProduct(null);
            }
        }

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(float $discount): self
    {
        $this->discount = $discount;

        return $this;
    }




}
