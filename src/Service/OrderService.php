<?php


namespace App\Service;


use App\Service\Deliverer\BaseDeliverer;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class OrderService
{
    /** @var SessionInterface */
    private $session;

    /** @var Security */
    private $security;

    /** @var CartService */
    private $cartService;


    private $delivererServices = [];

    /**
     * OrderService constructor.
     * @param SessionInterface $session
     * @param Security $security
     * @param CartService $cartService
     */
    public function __construct( $session,  $security,  $cartService)
    {
        $this->session = $session;
        $this->security = $security;
        $this->cartService = $cartService;
    }

    /**
     * @return SessionInterface
     */
    public function getSession(): SessionInterface
    {
        return $this->session;
    }

    /**
     * @param SessionInterface $session
     * @return OrderService
     */
    public function setSession(SessionInterface $session): OrderService
    {
        $this->session = $session;
        return $this;
    }

    /**
     * @return Security
     */
    public function getSecurity(): Security
    {
        return $this->security;
    }

    /**
     * @param Security $security
     * @return OrderService
     */
    public function setSecurity(Security $security): OrderService
    {
        $this->security = $security;
        return $this;
    }

    /**
     * @return CartService
     */
    public function getCartService(): CartService
    {
        return $this->cartService;
    }

    /**
     * @param CartService $cartService
     * @return OrderService
     */
    public function setCartService(CartService $cartService): OrderService
    {
        $this->cartService = $cartService;
        return $this;
    }


    public function addDeliverer(BaseDeliverer $deliverer)
    {
        $this->delivererServices[]=$deliverer;
    }



}