<?php


namespace App\Service\Listener;


use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class LocaleListener
{
    /** @var Session */
    private $session;

    /**
     * LocaleListener constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }


    public function onKernelRequest(RequestEvent $event)
    {
        $event->getRequest()->setLocale($this->session->get('_locale','ro'));
    }
}